package com.example.favmovie.Ultilty

import org.junit.Assert
import org.junit.Test
import java.text.ParseException


class GeneralUtilsKtTest {

    @Test(expected = ParseException::class)
    fun dateIsEmpty() {
        val result = convertDate("")
        AssertionError(result)
    }

    @Test(expected = ParseException::class)
    fun ifDateWrongFormat(){
        val result = convertDate("12/07/1994")
        AssertionError(result)
    }

    @Test
    fun ifDateCorrect(){
        //2020-07-24
        val result = convertDate("2020-07-24")
        Assert.assertEquals(result,"24 July 2020")
    }

    @Test
    fun ifDateisNull(){
        val result = convertDate(null)
        Assert.assertNull(result)
    }
}