package com.example.favmovie.Activity

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebSettings
import com.example.favmovie.R
import com.example.favmovie.Ultilty.Constants
import kotlinx.android.synthetic.main.activity_web_view.*

class WebViewActivity : BaseActivity() {

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        webview.settings.javaScriptEnabled = true
        webview.settings.pluginState = WebSettings.PluginState.ON
        webview.loadUrl(Constants.BOOK_MOVIE_URL)

        setStatusBarColor(R.color.colorPrimary)
        backBtn.setOnClickListener { onBackPressed() }
    }
}
