package com.example.favmovie.Activity

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.example.favmovie.Class.MovieDetailsClass
import com.example.favmovie.R
import com.example.favmovie.Ultilty.Constants
import com.example.favmovie.Ultilty.convertDate
import kotlinx.android.synthetic.main.activity_movie_details.*

class MovieDetailsActivity : BaseActivity() {

    private lateinit var movieDetailsClassObserver: Observer<MovieDetailsClass>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)
        setStatusBarColor(R.color.colorPrimary)
        setSupportActionBar(toolbar2)
        val title = intent.getStringExtra("movie_title")
        movieTitle.text = title
        movieTitle2.text = title
        val id = intent.getIntExtra("movie_id",0)
        if (supportActionBar != null) {
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            supportActionBar?.elevation = 0F
        }

        movieDetailsClassObserver = Observer {
            if(it != null){
                if(!it.getOverView().isNullOrEmpty()){
                    movieSynopsis.text = it.getOverView()
                }
                Glide.with(this)
                    .load(Constants.MOVIE_IMAGE + it.getImagePath())
                    .centerCrop()
                    .error(R.drawable.ic_no_image)
                    .into(imgMovie)
                movieGenre.text = resources.getString(R.string.genre).plus(
                    when(it.getGenre().isNullOrEmpty()){
                        true->{" No Genre"}
                        else->{
                            var temp: String =" "
                            it.getGenre()!!.forEach { genre-> temp += genre.getNameGenre() }
                            temp
                        }
                    }
                )
                var date = convertDate("17-04-1993")
                if(date.isNullOrEmpty()) date = "null"
                movieRelease.text = "Released : ${date}"
                movieLanguage.text = resources.getString(R.string.language).plus(
                    when(it.getLanguage().isNullOrEmpty()){
                        true->{" No Language"}
                        else->{
                            " "+ it.getLanguage()
                        }
                    }
                )
            }else{
                //smth wrong with the api
            }

        }
        rlBookMovie.setOnClickListener {
            val intent = Intent(this,WebViewActivity::class.java)
            startActivity(intent)
        }
        backBtn.setOnClickListener { onBackPressed() }
        movieViewModel.getMovieDetailsClass.observe(this,movieDetailsClassObserver)
        movieViewModel.fetchMovieDetails(id)

    }

}
