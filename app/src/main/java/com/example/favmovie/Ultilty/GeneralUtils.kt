package com.example.favmovie.Ultilty

import android.annotation.SuppressLint
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat

@SuppressLint("SimpleDateFormat")
@Throws(ParseException::class)
fun convertDate(date: String?): String? {

    val mSDF: DateFormat = SimpleDateFormat("d MMMM yyyy")
    //2020-07-24
    val formatter = SimpleDateFormat("yyyy-MM-dd")
    if(date == null) return null
    return mSDF.format(formatter.parse(date))
}
