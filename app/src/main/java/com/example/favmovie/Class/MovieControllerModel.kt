package com.example.favmovie.Class

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.favmovie.API.MovieAPI
import com.example.favmovie.API.Resp.MainGetDiscoverMovieResp
import com.example.favmovie.API.Resp.MovieDetailsResp
import com.example.favmovie.Ultilty.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import retrofit2.HttpException
import timber.log.Timber

class MovieControllerModel: ViewModel() {
    private var disposable: Disposable? = null
    private val movieApi by lazy {
        MovieAPI.createBaseURL(Constants.BASE_URL_TMDb)
    }

    var discoverMovieLiveData : MutableLiveData<MovieDiscoveryClass> = MutableLiveData()
    var getMovieDetailsClass: MutableLiveData<MovieDetailsClass> = MutableLiveData()

    fun fetchMovieList(page: String){
        disposable =  movieApi
            .getDiscoverMovie(Constants.API_KEY,Constants.MOVIE_LANG,Constants.MOVIE_SORT_BY,page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { onSuccess: MainGetDiscoverMovieResp? ->
                    onSuccess?.let {
                        val tempListMovie: MutableList<MovieClass> = mutableListOf()
                        it.results.forEach { movie-> tempListMovie.add(MovieClass(movie.id,movie.title,movie.popularity,movie.poster_path,movie.original_title)) }
                        val get = MovieDiscoveryClass(it.page,it.total_pages,tempListMovie)
                        discoverMovieLiveData.postValue(get)
                    }
                },
                { onError: Throwable ->
                    try {
                        val jsonObjError = JSONObject(
                            (onError as HttpException)
                                .response()!!
                                .errorBody()!!
                                .string()
                        )
                        Timber.e(onError)
                    } catch (e: Exception) {
                        Timber.e(e)
                    }
                }
            )
    }

    fun fetchMovieDetails(movieID:Int){
        disposable =  movieApi
            .getMovie(movieID,Constants.API_KEY,Constants.MOVIE_LANG)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { onSuccess: MovieDetailsResp? ->
                    onSuccess?.let {
                        val tempGenreList: MutableList<MovieGenre> = mutableListOf()
                        it.genres.forEach { genre-> tempGenreList.add(MovieGenre(genre.MovieGenreRespID,genre.MovieGenreRespName)) }
                        val get = MovieDetailsClass(it.overview,it.release_date,it.original_language,tempGenreList,it.poster_path)
                        getMovieDetailsClass.postValue(get)

                    }
                },
                { onError: Throwable ->
                    try {
                        val jsonObjError = JSONObject(
                            (onError as HttpException)
                                .response()!!
                                .errorBody()!!
                                .string()
                        )
                        Timber.e(onError)
                    } catch (e: Exception) {
                        Timber.e(e)
                    }
                }
            )
    }


}