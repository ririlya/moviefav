package com.example.favmovie.Class

class MovieDiscoveryClass {
    private var page: Int = 0
    private var totalPage: Int = 0
    private var listMovie: MutableList<MovieClass> = mutableListOf()

    constructor(page: Int, totalPage: Int, listMovie: MutableList<MovieClass>) {
        this.page = page
        this.totalPage = totalPage
        this.listMovie = listMovie
    }

    fun getListMovie(): MutableList<MovieClass> {
        return listMovie
    }

    fun getMaxPage(): Int {
        return totalPage
    }
}