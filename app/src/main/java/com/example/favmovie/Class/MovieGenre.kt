package com.example.favmovie.Class

class MovieGenre {
    private var id: String? = ""
    private var name :String? = ""

    constructor(id: String?, name: String?) {
        this.id = id
        this.name = name
    }

    fun getNameGenre(): String? {
        return name
    }
}