package com.example.favmovie.Class

class MovieClass {
    private var id: Int = 0
    private var title:String = ""
    private var popularity: Float = 0F
    private var picture_path: String? = ""
    private var original_title: String = ""

    constructor(
        id: Int,
        title: String,
        popularity: Float,
        picture_path: String?,
        original_title: String
    ) {
        this.id = id
        this.title = title
        this.popularity = popularity
        this.picture_path = picture_path
        this.original_title = original_title
    }

    fun getTitle():String{
        return title
    }
    fun getPopularity():Float{
        return popularity
    }

    fun getPicture():String?{
        return picture_path
    }

    fun getMovieId():Int{
        return id
    }

}