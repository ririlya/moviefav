package com.example.favmovie.Class

class MovieDetailsClass {
    private var overview: String? = ""
    private var releaseDate :String? = ""
    private var language: String? = ""
    private var genre: MutableList<MovieGenre>? = mutableListOf()
    private var poster_path : String? = ""

    constructor(
        overview: String?,
        releaseDate: String?,
        language: String?,
        genre: MutableList<MovieGenre>?,
        poster_path: String?
    ) {
        this.overview = overview
        this.releaseDate = releaseDate
        this.language = language
        this.genre = genre
        this.poster_path = poster_path
    }

    fun getOverView():String?{
        return overview
    }

    fun getImagePath():String?{
        return poster_path
    }

    fun getGenre(): MutableList<MovieGenre>? {
        return genre
    }

    fun getReleaseDate():String?{
        return releaseDate
    }

    fun getLanguage():String?{
        return language
    }
}