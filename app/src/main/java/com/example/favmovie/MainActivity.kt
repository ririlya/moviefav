package com.example.favmovie

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.example.favmovie.Activity.BaseActivity
import com.example.favmovie.Activity.MovieDetailsActivity
import com.example.favmovie.Adapter.MovieDiscoveryAdapter
import com.example.favmovie.Class.MovieClass
import com.example.favmovie.Class.MovieControllerModel
import com.example.favmovie.Class.MovieDiscoveryClass
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(),MovieDiscoveryAdapter.movieListener {

    private lateinit var movieObserver: Observer<MovieDiscoveryClass>
    private var listMovie: MutableList<MovieClass> = mutableListOf()
    private lateinit var listAdapter: MovieDiscoveryAdapter
    private var count:Int = 1
    private var isLoadingMore = false
    private var maxPage = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar2)
        setStatusBarColor(R.color.colorPrimary)
        if (supportActionBar != null) {
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            supportActionBar?.elevation = 0F
        }
        rvMovieDiscover.layoutManager =  LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvMovieDiscover.addItemDecoration(DividerItemDecoration(rvMovieDiscover.context, DividerItemDecoration.VERTICAL))

        rvMovieDiscover.setItemViewCacheSize(50)
        (rvMovieDiscover.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        listAdapter = MovieDiscoveryAdapter(this,listMovie,this)
        rvMovieDiscover.adapter = listAdapter
//        listAdapter.setHasStableIds(true)
        movieViewModel = ViewModelProvider(this).get(MovieControllerModel::class.java)
        count = 1
        movieViewModel.fetchMovieList(count.toString())
        movieObserver = Observer {
            //do smth to append the list
            if(it != null){
                maxPage = it.getMaxPage()
                swipe.isRefreshing = false
                isLoadingMore = false
                listMovie.addAll(it.getListMovie())
                listAdapter.notifyDataSetChanged()
            }
        }
        movieViewModel.discoverMovieLiveData.observe(this,movieObserver)
        swipe.setOnRefreshListener {
            count = 1
            listMovie.clear()
            swipe.isRefreshing = true
            isLoadingMore = true
            movieViewModel.fetchMovieList(count.toString())
        }

        rvMovieDiscover.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            @SuppressLint("ShowToast")
            override fun onScrolled(@NonNull recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?
                if (!isLoadingMore) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == listMovie.size - 1) {
                        isLoadingMore = true
                        count++
                        if(count> maxPage){
                            isLoadingMore = false
                            Toast.makeText(applicationContext,resources.getString(R.string.you_reach_end),Toast.LENGTH_LONG)
                        }else{
                            swipe.isRefreshing = true
                            movieViewModel.fetchMovieList(count.toString())
                        }

                    }
                }
            }
        })

    }

    override fun onMovieSelected(id: Int,title:String) {
        val intent = Intent(this, MovieDetailsActivity::class.java)
        intent.putExtra("movie_id",id)
        intent.putExtra("movie_title",title)
        startActivity(intent)
    }
}
