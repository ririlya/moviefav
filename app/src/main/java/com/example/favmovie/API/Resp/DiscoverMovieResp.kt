package com.example.favmovie.API.Resp

import com.google.gson.annotations.SerializedName

data class DiscoverMovieResp(
    @SerializedName("id") val id: Int,
    @SerializedName("popularity") val popularity: Float,
    @SerializedName("poster_path") val poster_path:String,
    @SerializedName("title") val title:String,
    @SerializedName("original_title") val original_title:String)

// "video": false,
//      "vote_average": 6.1,
//      "popularity": 2.651,
//      "vote_count": 45,
//      "release_date": "1874-12-09",
//      "adult": false,
//      "backdrop_path": "/d5O93YRHzzFTYv7B8DreUFDYd8t.jpg",
//      "overview": "Photo sequence of the rare transit of Venus over the face of the Sun, one of the first chronophotographic sequences.  In 1873, P.J.C. Janssen, or Pierre Jules César Janssen, invented the Photographic Revolver, which captured a series of images in a row. The device, automatic, produced images in a row without human intervention, being used to serve as photographic evidence of the passage of Venus before the Sun, in 1874.",
//      "genre_ids": [
//        99
//      ],
//      "title": "Passage of Venus",
//      "original_language": "xx",
//      "original_title": "Passage de Venus",
//      "poster_path": "/XWPDZzK7N2WQcejI8W96IxZEeP.jpg",
//      "id": 315946