package com.example.favmovie.API.Resp

import com.google.gson.annotations.SerializedName

data class MainGetDiscoverMovieResp(
    @SerializedName("page") val page: Int,
    @SerializedName("total_pages") val total_pages: Int,
    @SerializedName("results") val results: MutableList<DiscoverMovieResp>
)

//{
//  "page": 1,
//"results":[]
//  "total_results": 10000,
//"total_pages": 500
//}