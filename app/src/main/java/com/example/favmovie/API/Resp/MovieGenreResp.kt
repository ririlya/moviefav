package com.example.favmovie.API.Resp

import com.google.gson.annotations.SerializedName

data class MovieGenreResp(
    @SerializedName("id") val MovieGenreRespID: String,
    @SerializedName("name") val MovieGenreRespName: String
)