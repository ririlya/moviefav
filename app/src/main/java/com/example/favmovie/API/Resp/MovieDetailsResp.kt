package com.example.favmovie.API.Resp

import com.google.gson.annotations.SerializedName

data class MovieDetailsResp(
    @SerializedName("original_language") val original_language: String,
    @SerializedName("genres") val genres: MutableList<MovieGenreResp>,
    @SerializedName("release_date") val release_date: String,
    @SerializedName("overview") val overview: String,
    @SerializedName("poster_path") val poster_path:String
    )

//{
//  "adult": false,
//  "backdrop_path": "/3K2XHUb3MwBqKcLAKQupkQd9mRc.jpg",
//  "belongs_to_collection": null,
//  "budget": 0,
//  "genres": [
//    {
//      "id": 99,
//      "name": "Documentary"
//    }
//  ],
//  "homepage": "",
//  "id": 515791,
//  "imdb_id": "tt7986246",
//  "original_language": "en",
//  "original_title": "Woman Throwing Baseball",
//  "overview": "A series of photographic images by photographer Eadweard Muybridge, showing a nude woman picking up a baseball and throwing it.",
//  "popularity": 0.899,
//  "poster_path": "/1Lxz0Ff53MXC1sA5lGjHeFcoGuM.jpg",
//  "production_companies": [],
//  "production_countries": [
//    {
//      "iso_3166_1": "US",
//      "name": "United States of America"
//    }
//  ],
//  "release_date": "1886-12-31",
//  "revenue": 0,
//  "runtime": 1,
//  "spoken_languages": [
//    {
//      "english_name": "No Language",
//      "iso_639_1": "xx",
//      "name": "No Language"
//    }
//  ],
//  "status": "Released",
//  "tagline": "",
//  "title": "Woman Throwing Baseball",
//  "video": false,
//  "vote_average": 3.8,
//  "vote_count": 10
//}