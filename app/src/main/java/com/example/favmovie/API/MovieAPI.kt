package com.example.favmovie.API

import com.example.favmovie.API.Resp.MainGetDiscoverMovieResp
import com.example.favmovie.API.Resp.MovieDetailsResp
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface MovieAPI {

//    https://api.themoviedb.org/3/discover/movie?api_key=328c283cd27bd1877d9080ccb1604c91&language=en-US&sort_by=release_date.asc&include_adult=true&include_video=false&page=1
    @GET("discover/movie")
    fun getDiscoverMovie(
        @Query("api_key") api_key: String,
        @Query("language") language: String,
        @Query("sort_by") sort_by: String,
        @Query("page") page: String
    ): Observable<MainGetDiscoverMovieResp>

//    https://api.themoviedb.org/3/movie/515791?api_key=328c283cd27bd1877d9080ccb1604c91&language=en-US
    @GET("movie/{movieID}")
    fun getMovie(
        @Path("movieID") movieID: Int,
        @Query("api_key") api_key: String,
        @Query("language") language: String
    ): Observable<MovieDetailsResp>

    companion object {

        fun createBaseURL(baseUrl: String): MovieAPI {

            val clientBuilder = OkHttpClient.Builder()

            // OkHttp Logging interceptor
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            clientBuilder
                .addInterceptor(logging)

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientBuilder.build())
                .baseUrl(baseUrl)
                .build()

            return retrofit.create(MovieAPI::class.java)

        }
    }
}