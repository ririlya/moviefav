package com.example.favmovie.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.favmovie.Class.MovieClass
import com.example.favmovie.R
import com.example.favmovie.Ultilty.Constants

class MovieDiscoveryAdapter(private val mContext: Context, private var data: List<MovieClass>,private var mListener: movieListener) :
    RecyclerView.Adapter<MovieDiscoveryAdapter.ViewHolder>() {
    interface movieListener {
        fun onMovieSelected(id: Int,title:String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieDiscoveryAdapter.ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_movie_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: MovieDiscoveryAdapter.ViewHolder, position: Int) {
        holder.emjMovieTitle.text = data[position].getTitle()
        holder.emjMoviePopularity.text = data[position].getPopularity().toString()
        if(!data[position].getPicture().isNullOrEmpty()){
            Glide.with(mContext)
                .load(Constants.MOVIE_IMAGE + data[position].getPicture())
                .centerCrop()
                .error(R.drawable.ic_no_image)
                .into(holder.imgMovie)
        }
        holder.layoutMovie.setOnClickListener {
            mListener.onMovieSelected(data[position].getMovieId(),data[position].getTitle())
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal val layoutMovie : RelativeLayout = itemView.findViewById(R.id.rlMovie)
        internal val emjMovieTitle: TextView = itemView.findViewById(R.id.movieTitle)
        internal val emjMoviePopularity: TextView = itemView.findViewById(R.id.moviePopularity)
        internal val imgMovie: ImageView = itemView.findViewById(R.id.movieImg)
    }
}